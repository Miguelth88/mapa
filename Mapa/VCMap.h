//
//  ViewController.h
//  Mapa
//
//  Created by alumno on 8/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCMap : UIViewController

// Cambiar el tipo de mapa
- (IBAction)standardMap:(id)sender;
- (IBAction)satelliteMap:(id)sender;
- (IBAction)hybridMap:(id)sender;

// Notificaciones
// -(void) addedNewPoint: (NSNotification *) notification

@end

