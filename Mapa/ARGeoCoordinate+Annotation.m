//
//  ARGeoCoordinate+Annotation.m
//  Mapa
//
//  Created by alumno on 8/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import "ARGeoCoordinate+Annotation.h"


@implementation ARGeoCoordinate (Annotation)

- (CLLocationCoordinate2D) coordinate {
    return self.geoLocation.coordinate;
}

// (NSString *) title {
//    return self.dataObject;
// }

// - (void) setTitle:(NSString *)title {
//    self.title= title;
// }

@end
