//
//  VCTable.m
//  Mapa
//
//  Created by Miguel Ángel Tejedor Fernández on 31/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import "VCTable.h"
#import "ModeloCoordenadas.h"

@interface VCTable ()

@end

@implementation VCTable

//{
//    NSArray *content;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [content count];
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
//    
//    if (!cell)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
//    }
//    
//    NSString *cityName = [content objectAtIndex:indexPath.row];
//    [cell.textLabel setText:cityName];
//    
//    return cell;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [content objectAtIndex:indexPath.row];
    return cell;
}

// Para cuando se pica en una celda de la tabla, el equivalente a ir al detalle
//- (void)tableView:(UITableView *)tableView
//didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
   
//    NSLog(@"Seleccionaste el indice: %i", indexPath.row);
//}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    
    //Build a segue string based on the selected cell
//    NSString *segueString = [NSString stringWithFormat:@"%@Segue",
//                             [content objectAtIndex:indexPath.row]];
    //Since contentArray is an array of strings, we can use it to build a unique
    //identifier for each segue.
    
    //Perform a segue.
//    [self performSegueWithIdentifier:segueString
//                              sender:[content objectAtIndex:indexPath.row]];
    
    NSLog(@"Seleccionaste el indice: %li", (long)indexPath.row);
    
    [self performSegueWithIdentifier:@"tableDetailSegue"
                              sender:[content objectAtIndex:indexPath.row]];
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"tableDetailSegue"]) {
//        UIViewController *destinationVC = [segue destinationViewController];
//        
//        UITableViewCell *selectedCell = (UITableViewCell *)sender;
//        destinationVC.title = selectedCell.textLabel.text;
//    }
//}

//-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    if ([segue.identifier isEqualToString:@"tableDetailSegue"]) {
//        UIViewController *destinationVC=[segue destinationViewController];
//        
////        NSIndexPath* indexPath;
////        // ARGeoCoordinate *coordinate= (ARGeoCoordinate *) sender;
////        destinationVC.title=content[indexPath.row];
//        
//        UITableViewCell *selectedCell = (UITableViewCell *)sender;
//        destinationVC.title = selectedCell.textLabel.text;
//
//        
//    }
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"tableDetailSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        UIViewController *destinationVC = segue.destinationViewController;
        destinationVC.title = [content objectAtIndex:indexPath.row];
    }
}


///////////////////////////////////////////

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ModeloCoordenadas *coordenadas=[[ModeloCoordenadas alloc] init];
//    [self.tableView setDataSource: [coordenadas geoLocations]];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    content=[coordenadas tableLocations];
    // content=[NSArray arrayWithObjects:@[coordenadas], nil];
    NSLog(@"%@", content);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
