//
//  ARGeoCoordinate+Annotation.h
//  Mapa
//
//  Created by alumno on 8/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import "ARGeoCoordinate.h"
#import <MapKit/MapKit.h>

@interface ARGeoCoordinate (Annotation)<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
// @property (copy, nonatomic) NSString *title;

@end
