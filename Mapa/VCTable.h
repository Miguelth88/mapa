//
//  VCTable.h
//  Mapa
//
//  Created by Miguel Ángel Tejedor Fernández on 31/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCTable : UIViewController <UITableViewDataSource, UITableViewDelegate>

{
    NSArray *content;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
