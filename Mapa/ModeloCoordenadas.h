//
//  ModeloCoordenadas.h
//  Mapa
//
//  Created by Miguel Ángel Tejedor Fernández on 22/3/16.
//  Copyright © 2016 PEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARGeoCoordinate+Annotation.h"

@interface ModeloCoordenadas : NSObject

- (NSMutableArray *)geoLocations;

- (NSMutableArray *)tableLocations;

+ (ModeloCoordenadas *) singleton;

@end
